Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sigar
Source: https://rubygems.org/gems/sigar

Files: *
Copyright: 2004, 2006 Hyperic, Inc.
License: Apache-2.0

Files: bindings/SigarWrapper.pm bindings/ruby/rbsigar.c
Copyright: 2009-2010 VMware, Inc / 2009 SpringSource, Inc / 2007-2009 Hyperic, Inc
License: Apache-2.0

Files: bindings/ruby/extconf.rb
Copyright: 2010 VMware, Inc / 2009 SpringSource, Inc / 2007, 2009 Hyperic, Inc
License: Apache-2.0

Files: src/sigar_getline.c
Copyright: 1991, 1992 by Chris Thewalt (thewalt@ce.berkeley.edu)
License: other-getline
 Permission to use, copy, modify, and distribute this software
 for any purpose and without fee is hereby granted, provided
 that the above copyright notices appear in all copies and that both the
 copyright notice and this permission notice appear in supporting
 documentation.  This software is provided "as is" without express or
 implied warranty.

Files: bindings/SigarBuild.pm 
Copyright: 2009 VMware, Inc / 2009 SpringSource, Inc / 2009 Hyperic, Inc
License: Apache-2.0

Files: include/sigar.h 
Copyright: 2009-2010 VMware, Inc / 2009 SpringSource, Inc / 2004-2008 Hyperic, Inc
License: Apache-2.0

Files: include/sigar_util.h 
Copyright: 2009 SpringSource, Inc / 2004-2008 Hyperic, Inc
License: Apache-2.0

Files: include/sigar_fileinfo.h 
Copyright: 2000-2003 The Apache Software Foundation. All rights / 2004-2005 Hyperic, Inc
License: Apache-2.0

Files: include/sigar_log.h 
Copyright: 2004, 2006 Hyperic, Inc
License: Apache-2.0

Files: include/sigar_ptql.h 
Copyright: 2006-2007 Hyperic, Inc
License: Apache-2.0

Files: include/sigar_format.h 
Copyright: 2009 SpringSource, Inc / 2007-2008 Hyperic, Inc
License: Apache-2.0

Files: include/sigar_private.h 
Copyright: 2009-2010 VMware, Inc / 2009 SpringSource, Inc / 2004-2008 Hyperic, Inc
License: Apache-2.0

Files: src/sigar_cache.c 
Copyright: 2004-2006 Hyperic, Inc
License: Apache-2.0

Files: src/sigar_util.c 
Copyright: 2009 SpringSource, Inc / 2004-2009 Hyperic, Inc
License: Apache-2.0

Files: src/os/win32/win32_sigar.c 
Copyright: 2009-2010 VMware, Inc / 2009 SpringSource, Inc / 2004-2009 Hyperic, Inc
License: Apache-2.0

Files: src/os/win32/peb.c 
Copyright: 2009 VMware, Inc / 2004, 2006-2008 Hyperic, Inc / 2009 SpringSource, Inc
License: Apache-2.0

Files: src/os/win32/sigar_pdh.h 
Copyright: 2004, 2006 Hyperic, Inc
License: Apache-2.0

Files: src/os/win32/sigar_os.h 
Copyright: 2009-2010 VMware, Inc / 2009 SpringSource, Inc / 2004-2009 Hyperic, Inc
License: Apache-2.0

Files: src/os/aix/aix_sigar.c 
Copyright: 2009-2010 VMware, Inc / 2009 SpringSource, Inc / 2004-2009 Hyperic, Inc
License: Apache-2.0

Files: src/os/aix/sigar_os.h 
Copyright: 2010 VMware, Inc / 2004-2007, 2009 Hyperic, Inc / 2009 SpringSource, Inc
License: Apache-2.0

Files: src/os/linux/linux_sigar.c 
Copyright: 2009-2010 VMware, Inc / 2009 SpringSource, Inc / 2004-2009 Hyperic, Inc
License: Apache-2.0

Files: src/os/linux/sigar_os.h 
Copyright: 2004-2008 Hyperic, Inc
License: Apache-2.0

Files: src/os/darwin/darwin_sigar.c 
Copyright: 2009-2010 VMware, Inc / 2009 SpringSource, Inc / 2004-2009 Hyperic, Inc
License: Apache-2.0

Files: src/os/darwin/sigar_os.h 
Copyright: 2004-2006, 2008 Hyperic, Inc
License: Apache-2.0

Files: src/os/hpux/hpux_sigar.c 
Copyright: 2009-2010 VMware, Inc / 2009 SpringSource, Inc / 2004-2009 Hyperic, Inc
License: Apache-2.0

Files: src/os/hpux/sigar_os.h 
Copyright: 2004-2007 Hyperic, Inc
License: Apache-2.0

Files: src/os/solaris/kstats.c 
Copyright: 2009 SpringSource, Inc / 2004-2007 Hyperic, Inc
License: Apache-2.0

Files: src/os/solaris/solaris_sigar.c 
Copyright: 2009-2010 VMware, Inc / 2009 SpringSource, Inc / 2004-2008 Hyperic, Inc
License: Apache-2.0

Files: src/os/solaris/procfs.c 
Copyright: 2004, 2006 Hyperic, Inc
License: Apache-2.0

Files: src/os/solaris/get_mib2.c src/os/solaris/get_mib2.h
Copyright: 1995 Purdue Research Foundation. / 1995 Purdue Research Foundation, West Lafayette, Indiana
License: other-mib2
 Written by Victor A. Abell <abe@cc.purdue.edu>
 .
 This software is not subject to any license of the American Telephone
 and Telegraph Company or the Regents of the University of California.
 .
 Permission is granted to anyone to use this software for any purpose on
 any computer system, and to alter it and redistribute it freely, subject
 to the following restrictions:
 .
 1. Neither Victor A  Abell nor Purdue University are responsible for
    any consequences of the use of this software.
 .
 2. The origin of this software must not be misrepresented, either by
    explicit claim or by omission.  Credit to Victor A. Abell and Purdue
    University must appear in documentation and sources.
 .
 3. Altered versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 .
 4. This notice may not be removed or altered.

Files: src/os/solaris/sigar_os.h 
Copyright: 2009 SpringSource, Inc / 2004-2007 Hyperic, Inc
License: Apache-2.0

Files: src/sigar_ptql.c 
Copyright: 2010 VMware, Inc / 2006-2008 Hyperic, Inc / 2009 SpringSource, Inc
License: Apache-2.0

Files: src/sigar_format.c 
Copyright: 2010 VMware, Inc / 2009 SpringSource, Inc / 2007-2008 Hyperic, Inc
License: Apache-2.0

Files: src/sigar_signal.c 
Copyright: 2007 Hyperic, Inc
License: Apache-2.0

Files: src/sigar.c 
Copyright: 2009-2010 VMware, Inc / 2009 SpringSource, Inc / 2004-2009 Hyperic, Inc
License: Apache-2.0

Files: src/sigar_fileinfo.c 
Copyright: 2010 VMware, Inc / 2009 SpringSource, Inc / 2000-2003 The Apache Software Foundation. All rights / 2004-2005, 2007-2008 Hyperic, Inc
License: Apache-2.0

Files: debian/*
Copyright: 2012 Praveen Arimbrathodiyil <pravi.a@gmail.com>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache License version 2.0 can be
 found in the file `/usr/share/common-licenses/Apache-2.0'.

