Source: ruby-sigar
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               libtirpc-dev
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-sigar.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-sigar
Homepage: https://sigar.hyperic.com/
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-sigar
Architecture: any
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Multi-Arch: same
Description: System Information Gatherer And Reporter
 One API to access system information regardless of the underlying platform
 .
 Hyperic's System Information Gatherer (SIGAR) is a cross-platform API for
 collecting software inventory data. SIGAR is core of HQ's auto-discovery
 functionality, and you can use it to extend auto-discovery behavior.
 .
 SIGAR includes support for Linux, FreeBSD, Windows, Solaris, AIX, HP-UX and
 Mac OSX across a variety of versions and architectures. Users of the SIGAR
 API are given portable access to inventory and monitoring data including:
 .
  * System memory, swap, cpu, load average, uptime, logins
  * Per-process memory, cpu, credential info, state, arguments, environment,
    open files
  * File system detection and metrics
  * Network interface detection, configuration information and metrics
  * Network route and connection tables
 .
 This is ruby binding for the core API, which is implemented in pure C.
